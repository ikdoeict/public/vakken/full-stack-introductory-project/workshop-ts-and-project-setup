# Custom Types in TypeScript

## Type Aliases

Type aliases in TypeScript allow you to create your own custom types.

```typescript
type Name = string

const isItHim = function (search: Name): boolean {
    return (search === "John")
}

isItHim("Paul")
isItHim(42)
// -> ⛔ Argument of type 'number' is not assignable to parameter of type 'string'.
```

*I have never used type aliases like this.  
But it is called **Type**Script after all.  
There is a whole debate about types vs interfaces and it's worth reading up on.

See: [Differences between Type Aliases and interfaces](https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#differences-between-type-aliases-and-interfaces)
> For the most part, you can choose based on personal preference, 
> and TypeScript will tell you if it needs something to be the other kind of declaration. 
> If you would like a heuristic, use interface until you need to use features from type.

## Interfaces

Interfaces define custom data structures and contract for objects.  
They help with type checking and ensure consistency across your codebase.

```typescript
interface Person {
  name: string
  age: number
}
```

### Optional Properties

You can make properties optional by appending `?` to their names.

```typescript
interface User {
  username: string
  email?: string
}
```

### Extending Interfaces

Interfaces can extend other interfaces to inherit their properties.  
This promotes code reuse and keeps your codebase modular.

```typescript
interface Employee {
  employeeId: number
}

interface Manager extends Person, Employee {
  // Inherits properties from both Person and Employee
  role: string
}
```

## Where to Place Interface Definitions

It's a good practice to place interface definitions in a separate file or a dedicated `types.ts` file. This file can then be imported wherever the interfaces are needed.

:::tip Remember ES6 modules!
https://ikdoeict.gitlab.io/vakken/opo_front-end/js-slides/14.modules.html#/2/3
:::

### Example

```typescript
// types.ts
export interface Book {
  title: string
  author: string
}

// main.ts
import { Book } from './types'

const myBook: Book = { title: 'TypeScript 101', author: 'John Doe' }

const read = function(book: Book): Promise<void> {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(), 5000)
    })
}

read({title: "Harry Potter and the higher order functions", auteur: "robert galbraith"})
// -> ⛔ Object literal may only specify known properties, and 'auteur' does not exist in type 'Book'.
```

Custom types, whether defined through type aliases or interfaces, 
enhance code readability, maintainability, and help catch potential bugs during development.

## Exercises

:::info
You can use the [TypeScript playground](https://www.typescriptlang.org/play) for all exercises.
:::

```ts
/*
Exercise 1: Custom interface for Geographical Location

Create an interface named `Location` representing a geographical  
location with latitude and longitude as numbers.

*/

/*
Exercise 2: Interface for a Blog Post

Define an interface named `BlogPost` with the following properties:
- `title` (string)
- `content` (string)
- `author` (string)
- `published` (boolean)

*/

/*
Exercise 3: Extending Interfaces for Multimedia Posts

Create an interface named `MultimediaPost` that extends the `BlogPost`  
interface and adds properties:
- `mediaType` (string)
- `mediaUrl` (string)

*/

/*
Exercise 4: Type Alias for Configuration

Create a type alias named `Configuration` representing a configuration object  
with the following optional properties:
- `theme` (string)
- `fontSize` (number)
- `debugMode` (boolean)

*/

/*
Exercise 5: Interface for a Product

Define an interface named `Product` with properties:
- `productId` (string)
- `name` (string)
- `price` (number)

*/

/*
Exercise 6: Type Alias for a Function

Create a type alias named `Calculator` representing a function type that takes two numbers and returns a number.

*/

/*
Exercise 7: Interface for Social Media Profile

Create an interface named `SocialMediaProfile` with properties:
- `username` (string)
- `followers` (number)

*/

/*
Exercise 8: Type Alias for Union of Status

Create a type alias named `Status` representing a union of possible status values:
- `'active'`
- `'inactive'`
- `'pending'`

*/

/*
Exercise 9: Interface for a Playlist with Typed Songs

Define an interface named `Song` with properties:
- `title` (string)
- `artist` (string)

Define an interface named `Playlist` with properties:
- `name` (string)
- `songs` (an array of `Song`)
- `genre` (string)

*/


```