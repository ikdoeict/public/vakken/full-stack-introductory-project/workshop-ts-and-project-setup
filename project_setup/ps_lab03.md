# 3. Refactor task-api.ts

### task-api.ts

* Put refreshTodos(), removeTodo(), addTodo() and editTodo() in task-api.ts, export them
* Get rid of all TypeScript/ESLint/Prettier errors

To help you get started, here is the first part of 'refreshTodos'

```ts
export const refreshTodos = function (): void {
  fetch("/api/tasks")
    .then((response) => {
      return response.json() as Promise<{ tasks: Task[] }>
    })
    .then((todos) => {
     // Now TypeScript knows that 'todos' has the type { tasks: Task[] }    
    
```

⚠️ We just created a new error!  
`⛔ TS2304: Cannot find name 'Task'.`  
🔧 In types.ts create and export the interface Task

## types.ts

```ts
// Most interface use id and Name properties anyway, so reuse this as a base.
// It also reminds you about extends, so, that's nice.
interface IdNameObject {
    id: number,
    name: string
}

export interface Task extends IdNameObject{
    // the rest is up to you
}
```

## .env

The task-api functions use "/api/tasks/" as a hard coded string.  

This no longer works!  
The application is now decoupled: the API is hosted on another port/another domain.

You can update the string to "localhost:8080/api/tasks', but what if we build for production?

### Use .env files

* Read up on [env variables and modes](https://vitejs.dev/guide/env-and-mode)
* Create a `.env` and `.env.development` file in the root of your front-end project

Content of .env  
_(used in production, so at one point you'll have to fill it in)_
```dotenv
VITE_API_BASE_URL=
```
content of .env.development
```dotenv
VITE_API_BASE_URL=http://localhost:8080/api
```

And replace the hard coded strings by `import.meta.env.VITE_API_BASE_URL`  
````ts
  fetch("/api/tasks")                                // [!code --]  
  fetch(`${import.meta.env.VITE_API_BASE_URL}/tasks`)// [!code ++]

````