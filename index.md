---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Full Stack: introductory project"
  text: "Front-end setup "
  tagline: TypeScript & Vite
  actions:
    - theme: brand
      text: TypeScript
      link: /typescript/ts01
    - theme: brand
      text: Project setup
      link: /project_setup/ps01
features:
  - icon: 
      src: https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg
    title: Typescript
    link: https://www.typescriptlang.org/docs/handbook/intro.html
  - icon:
      src: https://upload.wikimedia.org/wikipedia/commons/f/f1/Vitejs-logo.svg
    title: Vite
    link: https://vitejs.dev/
  - icon:
      src: https://upload.wikimedia.org/wikipedia/commons/e/e3/ESLint_logo.svg
    title: ESLint
    link: https://eslint.org/
  - icon:
      src: https://prettier.io/icon.png
    title: Prettier
    link: https://prettier.io/
  - icon:
      light: 
        src: https://stylelint.io/img/light.svg
      dark: 
        src: https://stylelint.io/img/dark.svg
    title: Stylelint
    link: https://stylelint.io/
  - icon:
      src: https://pnpm.io/img/pnpm-no-name-with-frame.svg
    title: PNpM
    link: https://pnpm.io/
---