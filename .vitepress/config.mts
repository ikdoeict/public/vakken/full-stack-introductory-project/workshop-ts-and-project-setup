import {defineConfig} from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
    base: '/public/vakken/full-stack-introductory-project/workshop-ts-and-project-setup/',
    outDir: './public',
    title: "TypeScript & Vite",
    description: "Full Stack: introductory project - front-end setup ",
    themeConfig: {
        // https://vitepress.dev/reference/default-theme-config
        nav: [
            {text: 'Home', link: '/'},
            {text: 'TypeScript', link: '/typescript/ts01'},
            {text: 'Project setup', link: '/project_setup/ps01'},
        ],
        sidebar:  [
                {
                    text: 'TypeScript',
                    items: [
                        {text: '01 Getting started', link: '/typescript/ts01'},
                        {text: '02 Advanced Types', link: '/typescript/ts02'},
                        {text: '03 Custom Types', link: '/typescript/ts03'},
                        {text: '04 type assertions', link: '/typescript/ts04'},
                        {text: 'More TypeScript!', link: 'https://www.typescriptlang.org/docs/handbook/intro.html'},
                        {text: 'Playground', link: 'https://www.typescriptlang.org/play'}
                    ]
                },
                {
                    text: 'Project setup with vite',
                    items: [
                        {text: '01 pnpm', link: '/project_setup/ps01'},
                        {text: '02 vite', link: '/project_setup/ps02'},
                        {text: '03 ESLint', link: '/project_setup/ps03'},
                        {text: '04 Prettier', link: '/project_setup/ps04'},
                        {text: '05 Stylelint', link: '/project_setup/ps05'},
                        {text: '06 Vite again', link: '/project_setup/ps06'},
                        {text: '07 scripts', link: '/project_setup/ps07'},
                        {text: '08 more!', link: '/project_setup/ps08'}
                    ]
                },
                {
                    text: 'Lab',
                    items: [
                        {text: '00 Start', link: '/project_setup/ps_lab00'},
                        {text: '01 Directory', link: '/project_setup/ps_lab01'},
                        {text: '02 Utils', link: '/project_setup/ps_lab02'},
                        {text: '03 task-api, types & .env', link: '/project_setup/ps_lab03'},
                        {text: '04 Finish & test', link: '/project_setup/ps_lab04'},
                        {text: '05 More', link: '/project_setup/ps_lab05'}
                    ]
                }
        ]
    }
})
